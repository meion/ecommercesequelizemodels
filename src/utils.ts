import * as bcrypt from 'bcrypt';
import config from './config.json';
import { Sequelize } from 'sequelize';
import { User } from './Model/User';
import { EcommercePlugin, pluginInit } from './Model/Plugin';
import { Role, RoleType } from './Model/Role';
import { RoleMember } from './Model/RoleMember';

interface SuperUser{
    level: string;
}

export const sequelize = new Sequelize(`postgres://${config.username}:${config.password}@${config.url}:${config.port}/${config.database}`);

// export const createUser = async () => {
//     try {
//         let user = await User.create({
//             name:"admin",
//             password: "spacewalker"
//         });
//         console.log(user);

//     } catch (ex) {
//         console.error(ex);
//     }
// }


export const Login = async (username: string, plainTextPassword: string): Promise<boolean> => {
    try {
        // TODO : Find User
        const storedPassword = "";
        return await ValidatePassword(plainTextPassword, storedPassword);
    } catch(ex) {
        return false;
    }
}



export const HashPassword = async (plainTextPassword: string, saltRounds: number = 10): Promise<string> => {
    try {
        const salt = await GenerateSalt(saltRounds);
        const hashedPassword = await bcrypt.hash(plainTextPassword, salt);
        return hashedPassword;
    } catch(ex) {
        return ex;
    }
}
export const ValidatePassword = async (plainTextPassword: string, hashPassword: string) => {
    return await bcrypt.compare(plainTextPassword, hashPassword);
}

const GenerateSalt = (saltRounds: number): Promise<string> => {
    return new Promise( async (resolve, reject) => {
        try {
            resolve(await bcrypt.genSalt(saltRounds));
        } catch (ex) {
            reject(ex);
        }
    })
}

export const createTestPlugins = () => {
    const pluginNames = ["Plugin 1", "Plugin 2", "Plugin 3", "Plugin 4", "Plugin 5", "Plugin 6", "Plugin 7", "Plugin 8", "Plugin 9", "Plugin 10"];
    const migrated = true;
    let promises = [];
    for(let pluginName of pluginNames){
        const version = `${getRandom()}.${getRandom()}.${getRandom()}`;
        promises.push(EcommercePlugin.create({
            migrated: migrated,
            version: version,
            name: pluginName
        }));
    }
    return Promise.all(promises);
}
export const createTestUsers = () => {
    const users = ["Admin", "User", "Shopper"];
    let password = "123";
    let promises = [];
    for(let user of users){
        const pass = password + getRandom().toString() + getRandom().toString() + getRandom().toString();
        promises.push(User.create({
            name: user,
            password: pass
        }));
    }
    return Promise.all(promises);
}
export const createRoles = () => {
    const roles : RoleType[] = ["Read", "Write", "Execute"];
    let promises = [];
    for(const role of roles){
        promises.push(Role.create({
            type: role
        }));
    }
    return Promise.all(promises);
}
export const createRoleMembers = () => {
    return new Promise(async (resolve, reject) => {
    let users = await User.findAll();
    let roles = await Role.findAll();
    let plugins = await EcommercePlugin.findAll();
    let promises: Promise<any>[] = [];
    Promise.all([users, roles, plugins])
        .then(() => {
            for(let user of users){
                for(let plugin of plugins){
                    promises.push(RoleMember.create({
                        pluginId: plugin.id,
                        userId: user.id,
                        roleId: roles[Math.floor(Math.random() * roles.length)]?.id,
                    }));
                }
            }
            resolve(Promise.all(promises));
        });
    });
}
export const getRandom = (amount: number = 10) => {
    return Math.floor((Math.random() * amount) + 1);
}