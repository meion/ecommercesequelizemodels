import {HashPassword, ValidatePassword} from "./utils";
test("HashPassword test", async () =>{
    const plainPassword = "qwe123";
    const anotherPlainPassword = "Qwe123";
    const saltRounds = 10;
    const hash = await HashPassword(plainPassword, saltRounds);
    const trufy = await ValidatePassword(plainPassword, hash);
    const falsy = await ValidatePassword(anotherPlainPassword, hash);

    expect(trufy).toBe(true);
    expect(falsy).toBe(false);
});

test("HashPassword wrong saltrounds", async () =>{
    const plainPassword = "qwe123";
    const saltRounds = 10;
    const hash = await HashPassword(plainPassword, saltRounds);
    const trufy = await ValidatePassword(plainPassword, hash);
    const anotherHash = await HashPassword(plainPassword, saltRounds + 1);
    

    expect(trufy).toBe(true);
    expect(anotherHash !== hash).toBe(true);
});