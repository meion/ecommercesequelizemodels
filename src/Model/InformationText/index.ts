import { Optional, Model } from "sequelize/types";

export type TextType = 
    | "Short"
    | "Normal"
    | "Long";

interface TextAttributes {
    id:number;
    content: string;
    type: TextType;
}

interface TextCreationAttributes extends Optional<TextAttributes, "id"> {}

export class InformationText extends Model<TextAttributes, TextCreationAttributes> implements TextAttributes {
    id!: number;
    content!: string;
    type!: TextType;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}