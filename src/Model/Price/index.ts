import { Optional, Model } from "sequelize/types";

interface PriceAttributes {
    id:number;
    price: number;
}

interface PriceCreationAttributes extends Optional<PriceAttributes, "id"> {}

export class Price extends Model<PriceAttributes, PriceCreationAttributes> implements PriceAttributes {
    id!: number;
    price!: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

}