import {
    Sequelize, 
    Model,
    Optional,
    DataTypes,
    HasManyGetAssociationsMixin,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    Association
} from 'sequelize';
import { sequelize } from '../../utils';
import { RoleMember } from '../RoleMember';

interface PluginAttributes {
    id:number;
    name: string;
    version: string;
    migrated: boolean;
}

interface PluginCreationAttributes extends Optional<PluginAttributes, "id"> {}

export class EcommercePlugin extends Model<PluginAttributes, PluginCreationAttributes> implements PluginAttributes {
    id!: number;
    name!: string;
    version!: string;
    migrated!: boolean;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

    public readonly rolemembers?: RoleMember[];
    public getRoleMembers!: HasManyGetAssociationsMixin<RoleMember>;
    public addRoleMember!: HasManyAddAssociationMixin<RoleMember, number>;
    public hasRoleMembers!: HasManyAddAssociationMixin<RoleMember, number>;
    public countRoleMembers!: HasManyCountAssociationsMixin;
    public createRoleMember!:HasManyCreateAssociationMixin<RoleMember>;

    public static associations: {
        rolemembers: Association<EcommercePlugin, RoleMember>
    }
}
export const pluginInit = () => {
    try {
        EcommercePlugin.init({
            id: {
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            migrated: {
                type: DataTypes.BOOLEAN
            },
            name: {
                type: new DataTypes.STRING(128),
                unique: true
            },
            version: {
                type: new DataTypes.STRING(128)
            }
        }, {
            sequelize: sequelize,
            tableName: "plugins"
        });
        return true;
    } catch(ex) {
        return false;
    }
}