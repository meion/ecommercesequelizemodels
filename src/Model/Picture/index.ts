import { Optional, Model } from "sequelize/types";

interface PictureAttributes {
    id:number;
    name: string;
    content: string;

}

interface PictureCreationAttributes extends Optional<PictureAttributes, "id"> {}

export class Picture extends Model<PictureAttributes, PictureCreationAttributes> implements PictureAttributes {
    content!: string;
    id!: number;
    name!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}