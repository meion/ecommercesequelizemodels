import {
    Sequelize, 
    Model,
    Optional,
    DataTypes,
    HasManyGetAssociationsMixin,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    Association
} from 'sequelize';
import { HashPassword, sequelize } from '../../utils';
import { RoleMember } from '../RoleMember';

interface UserAttributes {
    id:number;
    name: string;
    password: string;
}

interface UserCreationAttributes extends Optional<UserAttributes, "id"> {

}


export class User extends Model<UserAttributes, UserCreationAttributes> implements UserAttributes  {
    public id!: number;
    public name!: string;
    public password!: string;
    public latestLogin!: Date;
    public locked!: boolean;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

    public readonly rolemembers?: RoleMember[];
    public getRoleMembers!: HasManyGetAssociationsMixin<RoleMember>;
    public addRoleMember!: HasManyAddAssociationMixin<RoleMember, number>;
    public hasRoleMembers!: HasManyAddAssociationMixin<RoleMember, number>;
    public countRoleMembers!: HasManyCountAssociationsMixin;
    public createRoleMember!:HasManyCreateAssociationMixin<RoleMember>;

    public static associations: {
        rolemembers: Association<User, RoleMember>
    }
}
// console.log(sequelize);
export const userInit = () => {
    try {
        User.init({
            id:{
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            name: {
                type: new DataTypes.STRING(128),
                unique: true
            },
            password: {
                type: new DataTypes.STRING(128)
            }
        }, {
            tableName: "users",
            sequelize: sequelize
        });
        User.beforeCreate(async (user, options) => {
            const hashedPassword = await HashPassword(user.password);
            user.password = hashedPassword;
        });
    } catch(ex) {
        return false;
    }
    
}