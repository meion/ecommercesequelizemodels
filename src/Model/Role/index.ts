import {
    Sequelize, 
    Model,
    Optional,
    DataTypes,
    HasManyGetAssociationsMixin,
    HasManyAddAssociationMixin,
    HasManyCountAssociationsMixin,
    HasManyCreateAssociationMixin,
    Association
} from 'sequelize';
import { sequelize } from '../../utils';
import { RoleMember } from '../RoleMember';

export type RoleType = 
    | "Read"
    | "Write"
    | "Execute";

interface RoleAttributes {
    id:number;
    type: RoleType;
}

interface RoleCreationAttributes extends Optional<RoleAttributes, "id"> {}

export class Role extends Model<RoleAttributes, RoleCreationAttributes> implements RoleAttributes {
    public id!: number;
    public type!: RoleType;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

    public readonly rolemembers?: RoleMember[];
    public getRoleMembers!: HasManyGetAssociationsMixin<RoleMember>;
    public addRoleMember!: HasManyAddAssociationMixin<RoleMember, number>;
    public hasRoleMembers!: HasManyAddAssociationMixin<RoleMember, number>;
    public countRoleMembers!: HasManyCountAssociationsMixin;
    public createRoleMember!:HasManyCreateAssociationMixin<RoleMember>;

    public static associations: {
        rolemembers: Association<Role, RoleMember>
    }
}
export const roleInit = () => {
    try {
        Role.init({
            id:{
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            type: {
                type: DataTypes.ENUM,
                values: ["Read", "Write", "Execute"]
            }
        }, {
            tableName: "roles",
            sequelize: sequelize
        });
        return true;
    } catch(ex) {
        return false;
    }
}
