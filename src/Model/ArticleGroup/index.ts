import { Optional, Model, HasManyGetAssociationsMixin, HasManyAddAssociationMixin, HasManyCountAssociationsMixin, HasManyCreateAssociationMixin, Association, HasOneGetAssociationMixin, HasOneCreateAssociationMixin, HasOneSetAssociationMixin, DataTypes } from "sequelize/types";
import { Article } from "../Article";
import { InformationText } from "../InformationText";
import { Picture } from "../Picture";
import { sequelize } from "../../utils";

interface ArticleGroupAttributes {
    id:number;
    name: string;
    priceModifier: number;
    parentId: number;
}

interface ArticleGroupCreationAttributes extends Optional<ArticleGroupAttributes, "id" | "priceModifier" | "parentId"> {}


export class ArticleGroup extends Model<ArticleGroupAttributes, ArticleGroupCreationAttributes> implements ArticleGroupAttributes {
    parentId!: number;
    priceModifier!: number;
    id!: number;
    name!: string;
    

    public readonly articles?: Article[];
    public getArticles!: HasManyGetAssociationsMixin<Article>;
    public addArticles!: HasManyAddAssociationMixin<Article, number>;
    public hasArticles!: HasManyAddAssociationMixin<Article, number>;
    public countArticles!: HasManyCountAssociationsMixin;
    public createArticles!:HasManyCreateAssociationMixin<Article>;

    public readonly pictures?: Picture[];
    public getPictures!: HasManyGetAssociationsMixin<Picture>;
    public addPictures!: HasManyAddAssociationMixin<Picture, number>;
    public hasPictures!: HasManyAddAssociationMixin<Picture, number>;
    public countPictures!: HasManyCountAssociationsMixin;
    public createPictures!:HasManyCreateAssociationMixin<Picture>;

    public readonly parent?: ArticleGroup;
    public getParent!: HasOneGetAssociationMixin<ArticleGroup>;
    public setParent!: HasOneSetAssociationMixin<ArticleGroup, number>;
    public createParent!: HasOneCreateAssociationMixin<ArticleGroupAttributes>;
    public hasParent!: HasOneSetAssociationMixin<ArticleGroup, number>;

    public readonly texts?: InformationText[];
    public getTexts!: HasManyGetAssociationsMixin<InformationText>;
    public addTexts!: HasManyAddAssociationMixin<InformationText, number>;
    public hasTexts!: HasManyAddAssociationMixin<InformationText, number>;
    public countTexts!: HasManyCountAssociationsMixin;
    public createTexts!:HasManyCreateAssociationMixin<InformationText>;

    public static associations: {
        articles: Association<ArticleGroup, Article>,
        pictures: Association<ArticleGroup, Picture>,
        memberOf: Association<ArticleGroup, ArticleGroup>,
        texts: Association<ArticleGroup, InformationText>
    }
}
ArticleGroup.init(
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        name: {
            type: new DataTypes.STRING(128),
            allowNull: false
        },
        parentId: {
            type: DataTypes.INTEGER.UNSIGNED
        },
        priceModifier: {
            type: DataTypes.FLOAT
        }
    }, 
    {
        sequelize: sequelize,
        tableName: 'articlegroups'
    }
);


// Article 
ArticleGroup.belongsToMany(Article, {through: 'ArticleGroupMemberships'});
Article.belongsToMany(ArticleGroup, {through: 'ArticleGroupMemberships'});

// InformationText
ArticleGroup.hasMany(InformationText, {
    foreignKey: 'id',
    constraints: false,
    scope: {
        type: 'articlegroup'
    },
    as: 'texts'
});
InformationText.belongsTo(ArticleGroup, { foreignKey: 'id', constraints: false});

// Picture
ArticleGroup.hasMany(Picture, {
    foreignKey: 'id',
    constraints: false,
    scope: {
        type: 'articlegroup'
    },
    as: 'pictures'
});
Picture.belongsTo(ArticleGroup, { foreignKey: 'id', constraints: false});

// Parent ArticleGroup
ArticleGroup.hasMany(ArticleGroup, {
    foreignKey: 'parentId',
    foreignKeyConstraint: true
});
ArticleGroup.belongsTo(ArticleGroup);