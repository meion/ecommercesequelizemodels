import { Optional, Model, HasManyGetAssociationsMixin, HasManyAddAssociationMixin, HasManyCountAssociationsMixin, HasManyCreateAssociationMixin, Association, DataTypes } from "sequelize/types";
import { InformationText } from "../InformationText";
import { Price } from "../Price";
import { Picture } from "../Picture";
import { sequelize } from "../../utils";
import { ArticleGroup } from "../ArticleGroup";



interface ArticleAttributes {
    id:number;
    name: string;
}

interface ArticleCreationAttributes extends Optional<ArticleAttributes, "id"> {}

export class Article extends Model<ArticleAttributes, ArticleCreationAttributes> implements ArticleAttributes {
    id!: number;
    name!: string;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;

    public readonly texts?: InformationText[];
    public getTexts!: HasManyGetAssociationsMixin<InformationText>;
    public addTexts!: HasManyAddAssociationMixin<InformationText, number>;
    public hasTexts!: HasManyAddAssociationMixin<InformationText, number>;
    public countTexts!: HasManyCountAssociationsMixin;
    public createTexts!:HasManyCreateAssociationMixin<InformationText>;

    public readonly prices?: Price[];
    public getPrices!: HasManyGetAssociationsMixin<Price>;
    public addPrices!: HasManyAddAssociationMixin<Price, number>;
    public hasPrices!: HasManyAddAssociationMixin<Price, number>;
    public countPrices!: HasManyCountAssociationsMixin;
    public createPrices!:HasManyCreateAssociationMixin<Price>;

    public readonly pictures?: Picture[];
    public getPictures!: HasManyGetAssociationsMixin<Picture>;
    public addPictures!: HasManyAddAssociationMixin<Picture, number>;
    public hasPictures!: HasManyAddAssociationMixin<Picture, number>;
    public countPictures!: HasManyCountAssociationsMixin;
    public createPictures!:HasManyCreateAssociationMixin<Picture>;

    public readonly groups?: ArticleGroup[];
    public getGroups!: HasManyGetAssociationsMixin<ArticleGroup>;
    public addGroups!: HasManyAddAssociationMixin<ArticleGroup, number>;
    public hasGroups!: HasManyAddAssociationMixin<ArticleGroup, number>;
    public countGroups!: HasManyCountAssociationsMixin;
    public createGroups!:HasManyCreateAssociationMixin<ArticleGroup>;

    public static associations: {
        texts: Association<Article, InformationText>,
        prices: Association<Article, Price>,
        pictures: Association<Article, Picture>,
        groups: Association<Article, ArticleGroup>
    }
}
Article.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: new DataTypes.STRING(128)
    }
}, {
    sequelize: sequelize,
    tableName: "articles"
});

// InformationText
Article.hasMany(InformationText, {
    foreignKey: 'id',
    constraints: false,
    scope: {
        type: 'article'
    },
    as: 'texts'
});
InformationText.belongsTo(Article, { foreignKey: 'id', constraints: false});

// Price
Article.hasMany(Price, {
    sourceKey: "id",
    foreignKey: "articleId",
    as: "prices"
});
Price.belongsTo(Article, { targetKey: "id", foreignKey: "articleId"});

// Picture
Article.hasMany(Picture, {
    foreignKey: 'id',
    constraints: false,
    scope: {
        type: 'article'
    },
    as: 'pictures'
});
Picture.belongsTo(Article, { foreignKey: 'id', constraints: false});

