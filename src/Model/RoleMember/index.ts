import {
    Sequelize, 
    Model,
    Optional,
    DataTypes
} from 'sequelize';
import { sequelize } from '../../utils';
import { User } from '../User';
import { Role } from '../Role';
import { EcommercePlugin } from '../Plugin';

interface RoleMemberAttributes {
    id:number;
    userId: number;
    pluginId: number;
    roleId: number;
}

interface RoleMemberCreationAttributes extends Optional<RoleMemberAttributes, "id"> {}

export class RoleMember extends Model<RoleMemberAttributes, RoleMemberCreationAttributes> implements RoleMemberAttributes {
    id!: number;
    userId!: number;
    pluginId!: number;
    roleId!: number;

    public readonly createdAt!: Date;
    public readonly updatedAt!: Date;
}
export const roleMemberinit = () => {
    try {
        RoleMember.init({
            id:{
                type: DataTypes.INTEGER.UNSIGNED,
                autoIncrement: true,
                primaryKey: true
            },
            userId: {
                type: DataTypes.INTEGER.UNSIGNED
            },
            pluginId: {
                type: DataTypes.INTEGER.UNSIGNED
            },
            roleId: {
                type: DataTypes.INTEGER.UNSIGNED
            }
        }, {
            tableName: "roleMembers",
            sequelize: sequelize,
            indexes: [{
                unique:true,
                fields: ['userId', 'pluginId', 'roleId']
            }]
        });
        RoleMember.belongsTo(User, { targetKey: "id", foreignKey: "userId"});
        User.hasMany(RoleMember, {
            sourceKey: "id",
            foreignKey: "userId",
            as: "rolemembers"
        });
        RoleMember.belongsTo(EcommercePlugin, { targetKey: "id", foreignKey: "pluginId"});
        EcommercePlugin.hasMany(RoleMember, {
            sourceKey: "id",
            foreignKey: "pluginId",
            as: "plugins"
        });
        RoleMember.belongsTo(Role, { targetKey: "id", foreignKey: 'roleId'});
        Role.hasMany(RoleMember, {
            sourceKey: "id",
            foreignKey: "roleId",
            as: "rolemembers"
        });
        return true;
    } catch (ex) {
        return false;
    }
}
