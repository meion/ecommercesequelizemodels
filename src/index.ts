// import { createUser } from "./utils";
import { sequelize, createTestPlugins, createTestUsers, createRoles, createRoleMembers } from './utils';
import { User, userInit } from './Model/User';
import { roleInit } from './Model/Role';
import { roleMemberinit } from './Model/RoleMember';
import { pluginInit } from './Model/Plugin';






function init() {
    return new Promise((resolve, reject) => {
        let inits = [roleMemberinit, userInit, roleInit, pluginInit];
        let invalidInits = [];
        for(let init of inits){
            const initiated = init();
            if(!initiated){
                invalidInits.push(init);
            }
        }
        let tries = invalidInits.length;
        while(invalidInits.length !== 0 && tries > 0){
            const init = invalidInits.pop();
            const initated = init!();
            if(!initated){
                tries--;
                invalidInits = [init, ...invalidInits];
            }
        }
        sequelize.sync({force: true})
            .then(() => {
                let promises: Promise<any>[] = [createTestPlugins(), createTestUsers(), createRoles()];
                Promise.all(promises)
                    .then(async () => {
                        resolve(await createRoleMembers());
                    })
                    .catch((ex) => {
                        console.log(ex);
                    })
            })
            .catch((ex) => {
                console.log(ex);
            })
    })   
}

(function(){
    init()
        .then(() => {
            console.log("Done")
        })   
})();